#!/bin/bash

# -c           attempt to preserve all changed files in /etc/
# -n           do not save configuration over reflash

BIN=openwrt-ramips-rt305x-dir-300-b1-squashfs-sysupgrade.bin
LAN=root@openwrt.lan

tar -c $BIN | ssh -C $LAN "echo full reboot takes 10 minutes && date && tar -xv -C /tmp && sh --login -i /sbin/sysupgrade $* /tmp/$BIN" || exit 1
