#!/bin/sh

# sorry google ;)
HOST=google.com
SLEEP=15
LED_RED=d-link\:amber\:wan
LED_GREEN=d-link\:green\:wan
WAN=eth0.2
RETRY=3

led_on() {
  sysfs=$1

  echo 1 >/sys/class/leds/${sysfs}/brightness
}

led_off() {
  sysfs=$1

  echo 0 >/sys/class/leds/${sysfs}/brightness
}

green() {
  led_on $LED_GREEN
  led_off $LED_RED
  track_on $LED_GREEN
}

red() {
  track_off $LED_GREEN
  led_off $LED_GREEN
  led_on $LED_RED
}

off() {
  track_off $LED_GREEN
  led_off $LED_GREEN
  led_off $LED_RED
}

track_on() {
  sysfs=$1
  
  echo "netdev" > /sys/class/leds/${sysfs}/trigger
  echo $WAN > /sys/class/leds/${sysfs}/device_name
  echo "link tx rx" > /sys/class/leds/${sysfs}/mode
}

track_off() {
  sysfs=$1
  
  echo "none" > /sys/class/leds/${sysfs}/trigger
}

ping_r() {
  R=$1

  ping -c 1 $HOST > /dev/null 2>&1
  RET=$?
 
  if [ $RET -eq 0 ]; then
    return 0
  fi

  if [ $R -gt 0 ]; then
    ping_r $(($R-1))
    RET=$?

    return $RET
  else:
    return 1
  fi
}

# on exit turn the indicator off
trap "off && exit 0" EXIT SIGINT SIGTERM

while sleep "$SLEEP"; do
  ping_r $RETRY
  
  if [ $RET -eq 0 ]; then
    green
  else
    red
  fi
done
