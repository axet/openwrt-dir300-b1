#!/bin/bash

(cd trunk && ./scripts/feeds install -d y tcpdump) || exit 1
(cd trunk && ./scripts/feeds install -d y iptraf) || exit 1
(cd trunk && ./scripts/feeds install -d y pptpd) || exit 1
(cd trunk && ./scripts/feeds install -d y miniupnpd) || exit 1
(cd trunk && ./scripts/feeds install -d y igmpproxy) || exit 1
(cd trunk && ./scripts/feeds install -d y ddns-scripts) || exit 1

(cd trunk && cp ../config-openwrt .config) || exit 1
(cd trunk && svn revert target/linux/ramips/rt305x/config-3.10 && cat ../config-kernel >> target/linux/ramips/rt305x/config-3.10) || exit 1
(cd trunk && make defconfig) || exit 1
(rsync -ar --delete files/ trunk/files/) || exit 1
