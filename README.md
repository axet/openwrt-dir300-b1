I [moved](https://github.com/axet/openwrt-tl-wr842nd-v2) to tl-wr842nd usb router. Latest configs, init / build script can be found there.

# openwrt

  * Super stable!
  * Super modular architecture!
  * Super supportive community!
  * Super awesome product!

Learn from them! Join!

  * http://openwrt.org

# How to install

  1. ./clone.sh
  2. ./up.sh $REV
  3. ./make.sh

versions:

  * $REV = r39098 - (2013.12.21) (wifi issue)
  * $REV = r38094 - just moved on (2013.09.22) (wifi lose connection)
  * $REV = r37745 - just moved on (2013.08.10) (wifi lose connection)
  * $REV = r37015 - just moved on (2013.06.23) (wifi lose connection)
  * $REV = r36937 - (wifi lose connection)
  * $REV = r36884 - just moved on (2013.06.09) (wifi lose connection)
  * $REV = r36822 - wifi ok?
  * $REV = r36115 - wifi ok?
  * $REV = r35671 - just moved on (2013.02.19)
  * $REV = r35310 - just moved on (2013.01.24)
  * $REV = r33300 - serve me for a 6 months with no issues on my DIR-300-B1

Flash your DIR-300-B1 router and enjoy.

## How to Flash

1) take fresh file which you just build running ./make.sh

./openwrt-ramips-rt305x-dir-300-b1-squashfs-factory.bin

2) go to web interface of your router and flash it using this file.

## How to Rollback

1) http://ftp.dlink.ru/pub/Router/DIR-300_NRU/Firmware/

2) Power off DIR-300

3) Hold on to the reset button and power DIR-300 on

4) Hold on to the reset button for about 30 seconds while DIR-300 is booting

5) Goto http://192.168.0.1 and flush dir300b_*.bin

# Services enabled

  * UPnP (miniupnpd)
  * VPN (openvpn)
  * IPTV (igmproxy)
  * WiFi (http://wiki.openwrt.org/doc/uci/wireless)
  * DHCP (dhcpd)
    * 192.168.54.100-250
  * SSH Server (sshd)
  * Time Sync (ntpd)
  * Internet Ping ( <i>/etc/init.d/internet_ping</i>  )
  * Weekly Reboot ( _/etc/crontabs/root_ )

# Configs

Following config shall be changed before issue a '../up && ./make.sh'. Those steps are nessesery mostly because those
configs holds credentials / passwords or specific settings filled by your ISP.

So please read carrefuly.

## network setup

Original config + my net and IPS setup. More info at:
  * http://wiki.openwrt.org/doc/uci/network

You need to use your 'eth0.2' mac address.

**_/etc/config/network_**

    config interface 'wan'
        option ifname 'eth0.2'
        option proto 'static'
        option ipaddr 'xxx.xxx.21.162'
        option netmask '255.255.255.224'
        option gateway 'xxx.xxx.21.161'
        option macaddr 'xx:xx:xx:xx:xx:xx'
        option dns '8.8.8.8 8.8.4.4'

## wifi setup

Pretty original config + my ssid name and password. Don't forget to change macaddr option, which is nessesery
to lookup interface. Or just skip it, after login run 'wifi' will configure your wifi automatically. You have to
use 'eth0 or wlan0' interface mac address for proper interface lookup.

  * http://wiki.openwrt.org/doc/uci/wireless

**_/etc/config/wireless_**

    config wifi-device  radio0
        option macaddr  xx:xx:xx:xx:xx:xx
  
    config wifi-iface
        option device   radio0
        option network  lan
        option mode     ap
        option ssid     wifi.local
        option encryption 'psk2'
        option key      '12345678'

## VPN service

You can use booth openvpn and pptp vpns of you choose.

### openvpn

You have to prepare followings files in files/etc/openvpn/

    openssl dhparam -out dh1024.pem 1024

    openssl genrsa -out ca.key 4096
    openssl req -new -x509 -days 3650 -key ca.key -out ca.crt

    openssl genrsa -out server.key 4096
    openssl req -new -key server.key -out server.csr
    openssl x509 -req -days 3650 -in server.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out server.crt

    openssl genrsa -out client.key 4096
    openssl req -new -key client.key -out client.csr
    openssl x509 -req -days 3650 -in client.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out client.crt

**_client.conf_**

    client
    dev tap

    proto udp
    remote openwrt.lan 1194
    resolv-retry infinite

    nobind

    persist-key
    persist-tun


    ca ca.crt
    cert client.crt
    key client.key
    comp-lzo

    script-security 2

### pptp

Set your pptp range, and your users name and pasword:

**_files/etc/config/pptpd_**

    config service 'pptpd'
            option 'enabled' '0'
            option 'localip' '192.168.54.1'
            option 'remoteip' '192.168.54.20-30'
                            
    config 'login'
            option 'username' 'user1'
            option 'password' 'password1'
        

Fix firewall

Set your ipv4 network mask here, if changed:

**_files/etc/firewall.pptp_**

    # allow pptp vpn to use local network
    iptables        -A forwarding_rule -s 192.168.54.0/24 -d 192.168.54.0/24 -j ACCEPT
    
    # allow pptp vpn to use internet
    iptables        -A forwarding_rule -i ppp+ -o eth0.2 -j ACCEPT

## IPTV

Pretty original config. You have to check `logread` to see your ISP altnet mask. And allow firewall to pass
igmp trafic.

  * http://wiki.openwrt.org/doc/howto/udp_multicast

**_/etc/config/igmpproxy_**

    config phyint
        option network wan
        option direction upstream
        list altnet 77.94.170.0/24

## ntpd

Please fill up your timezone in the following config.

  * http://wiki.openwrt.org/doc/uci/system

**_/etc/config/system_**

    config system
        option hostname 'OpenWrt'
        option timezone 'MSK-4'

## ssh easy acess

Don't forget to make your live easier. Put your id-rsa.put into authorized_keys on the router.

**<i>/etc/dropbear/authorized_keys</i>**

    [your key]
    
