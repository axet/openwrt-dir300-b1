# Init

1) ./clone.sh

2) ./up.sh

3) ./make.sh

Flash your DIR-300-B1 router and enjoy.

# Services enabled

  * ddns
  * upnp
  * pptp
    * 192.168.54.50-100
  * igmproxy
  * dhcpd
    * 192.168.54.100-250
  * ssh
  * ntpd
  * weekly reboot

# Configs

## network setup

Original config + my net and IPS setup. More info at:
  * http://wiki.openwrt.org/doc/uci/network

**_/etc/config/network_**

    config interface 'loopback'
        option ifname 'lo'
        option proto 'static'
        option ipaddr '127.0.0.1'
        option netmask '255.0.0.0'

    config interface 'lan'
        option ifname 'eth0.1'
        option type 'bridge'
        option proto 'static'
        option ipaddr '192.168.54.1'
        option netmask '255.255.255.0'
        option macaddr 'xx:xx:xx:xx:xx:xx'

    config interface 'wan'
        option ifname 'eth0.2'
        option proto 'static'
        option ipaddr 'xxx.xxx.21.162'
        option netmask '255.255.255.224'
        option gateway 'xxx.xxx.21.161'
        option macaddr 'xx:xx:xx:xx:xx:xx'
        option dns '8.8.8.8 8.8.4.4'

**_/etc/config/dhcp_**

    config dnsmasq
        option domainneeded	1
        option boguspriv	1
        option filterwin2k	0  # enable for dial on demand
        option localise_queries	1
        option rebind_protection 1  # disable if upstream must serve RFC1918 addresses
        option rebind_localhost 1  # enable for RBL checking and similar services
        #list rebind_domain example.lan  # whitelist RFC1918 responses for domains
        option local	'/lan/'
        option domain	'lan'
        option expandhosts	1
        option nonegcache	0
        option authoritative	1
        option readethers	1
        option leasefile	'/tmp/dhcp.leases'
        option resolvfile	'/tmp/resolv.conf.auto'

    config dhcp lan
        option interface	lan
        option start 	100
        option limit	150
        option leasetime	2m

    config dhcp wan
        option interface	wan
        option ignore	1
        
## wifi setup

Pretty original config + my ssid name and password.

  * http://wiki.openwrt.org/doc/uci/wireless

**_/etc/config/wireless_**

    config wifi-device  radio0
        option type     mac80211
        option channel  11
        option macaddr	xx:xx:xx:xx:xx:xx
        option hwmode	11ng
        option htmode	HT20
        list ht_capab	GF
        list ht_capab	SHORT-GI-20
        list ht_capab	SHORT-GI-40
        list ht_capab	RX-STBC1

    config wifi-iface
        option device   radio0
        option network  lan
        option mode     ap
        option ssid     wifi.local
        option encryption 'psk2'
        option key      '12345678'

## reboot service

Allow you to reboot router every 3 am as saturday.

**_/etc/crontabs/reboot_**

    5 3 * * 6 /sbin/reboot

## ddns setup

Allow you to find your router by name with dynamic ip. Fill up your login, host and password.

  * http://wiki.openwrt.org/doc/howto/ddns.client

**_/etc/config/ddns_**

    config service "myddns"
        option enabled		"1"
        option interface	"wan"

        option service_name	"dyndns.org"
        option domain		"HOST.DYNDNS.ORG"
        option username		"LOGIN"
        option password		"PASSWORD"

        option ip_source	"network" 
        option ip_network	"wan"
	

        option force_interval	"72"
        option force_unit	"hours"
        option check_interval	"10"
        option check_unit	"minutes"
        option retry_interval	"60"
        option retry_unit	"seconds"

## Date and Time

Allow your router to know real time. Setup 'timezone'.

  * http://wiki.openwrt.org/doc/uci/system

**_/etc/config/system_**

    config system
        option hostname 'OpenWrt'
        option timezone 'MSK-4'

    config timeserver 'ntp'
        list server '0.openwrt.pool.ntp.org'
        list server '1.openwrt.pool.ntp.org'
        list server '2.openwrt.pool.ntp.org'
        list server '3.openwrt.pool.ntp.org'
        option enable_server '0'

    config led 'led_wan'
        option name 'WAN LED (amber)'
        option sysfs 'd-link:amber:wan'
        option default '1'
        
## VPN service

To allow pptp connections to your router you need setup configs, and allow firewall to pass the connection.
Don't forget to setup proxyarp variable. And allow 'lan' trafic forwarding.

  * http://wiki.openwrt.org/doc/howto/vpn.server.pptpd
        
**_/etc/pptpd.conf_**

    #debug
    option /etc/ppp/options.pptpd
    speed 115200
    stimeout 10

    bcrelay br-lan

    localip 192.168.54.1
    remoteip 192.168.54.50-100
    
**_/etc/ppp/chap-secrets_**

    #USERNAME  PROVIDER  PASSWORD  IPADDRESS
    user_name       *         123456    *

**_/etc/ppp/options.pptpd_**

    #debug
    #logfile /tmp/pptp-server.log
    172.16.1.1:
    auth
    name "pptp-server"
    lcp-echo-failure 3
    lcp-echo-interval 60
    default-asyncmap
    mtu 1482
    mru 1482
    nobsdcomp
    nodeflate
    proxyarp
    #nomppc
    mppe required,no40,no56,stateless
    require-mschap-v2
    refuse-chap
    refuse-mschap
    refuse-eap
    refuse-pap
    #ms-dns 172.16.1.1
    #plugin radius.so
    #radius-config-file /etc/radius.conf

**_/etc/config/firewall_**

    # allow pptp local forwarding
    config zone
        option name		lan
        option network		'lan'
        option input		ACCEPT
        option output		ACCEPT
        option forward		ACCEPT
        
    # allow pptp clients to connect
    config rule
        option src wan
        option dest_port 1723
        option target ACCEPT

## IPTV

Pretty original config. You have to check `logread` to see your ISP altnet mask. And allow firewall to pass
igmp trafic.

  * http://wiki.openwrt.org/doc/howto/udp_multicast

**_/etc/config/igmpproxy_**

    config igmpproxy
        option quickleave 1

    config phyint
        option network wan
        option direction upstream
        list altnet 77.94.170.0/24

    config phyint
        option network lan
        option direction downstream

**_/etc/config/firewall_**

    # allow iptv to work
    config rule
        option src	wan
        option proto    igmp
        option target   ACCEPT
                      
    config rule
        option src wan
        option proto udp
        option dest lan
        option dest_ip  224.0.0.0/4
        option target   ACCEPT

## ssh easy acess

Don't forget to make your live easier. Put your id-rsa.put into authorized_keys on the router.

**_/etc/dropbear/authorized_keys_**

    [your key]
    