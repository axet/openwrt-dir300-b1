#!/bin/bash

backup() {
  FILE=$1
  DATE=$(date -r $FILE +%F)
  mv $FILE $FILE.$DATE
}

(cd trunk && make $*) || exit 1

# automatic backup (always forgetting do it my self)

for i in *.bin; do
  backup $i
done

cp ./trunk/bin/ramips/openwrt-ramips-rt305x-dir-300-b1* . || exit 1
