#!/bin/bash

REV=$1

if [ -z "$REV" ]; then
  (cd trunk && svn up) || exit 1
  (cd trunk && ./scripts/feeds update -a) || exit 1
else
  (cd trunk && svn up -r $REV) || exit 1
  (cd trunk/feeds/packages && svn up -r $REV) || exit 1 
  (cd trunk && ./scripts/feeds update -i) || exit 1
fi

./install.sh || exit 1
